const fs = require('fs');

const Logs = {};

Logs.createLog = function createLog(name, content) {
	if (!name) { throw new Error('[Raven-Logger]: ' + 'It is not possible to create a log with an empty name.'); }
	else if (!content) { throw new Error('[Raven-Logger]: ' + 'You cannot create an empty log.'); }
	else {
		const stream = fs.createWriteStream(`${name}.log`, { flags:'a' });
		stream.write(content + '\n');
	}
};

module.exports = Logs;
