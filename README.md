# Raven logs

[Original Here](https://www.npmjs.com/package/txt-logs) Licensed under the MIT License

# Usage
require the package first:
```javascript
const { createLog } = require('@raven-studio/logs')
```
Now we can create logs:
```javascript
createLog('name', 'content')
```
# Example

```javascript
createLog('blah', 'blah blah logs') //  logs to blah.logs
```
